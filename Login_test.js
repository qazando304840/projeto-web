Feature('Login');

Scenario('Login com sucesso',  ({ I }) => {
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.fillField('#user','Vinicius_cleber@hotmail.com')
    I.fillField('#password','123456' )
    I.click('#btnLogin')
    I.waitForText('Login realizado',3)
}).tag('@sucesso')

Scenario('Tentando Logar digitando apenas o e-mail',  ({ I }) => {
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.fillField('#user','Vinicius_cleber@hotmail.com')
    I.click('#btnLogin')
    I.waitForText('Senha inválida.',3)

}).tag('@E')

Scenario('Tentando logar sem digitar e-mail e senha',  ({ I }) => {
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.',3)

}).tag('@SES')

Scenario('Tentando Logar digitando apenas a senha',  ({ I }) => {
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.fillField('#password','123456' )
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.',3)

}).tag('@S')

Scenario('Cadastro com sucesso',  ({ I }) => {
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Cadastro')
    I.fillField('#user','Vinicius' )
    I.fillField('#email','Vinicius_cleber@hotmail.com' )
    I.fillField('#password','123456' )
    I.click('#btnRegister')
    I.waitForText('Cadastro realizado!',3)

}).tag('@Teste')

Scenario('Cadastro com sucesso',  ({ I }) => {
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Cadastro')
    I.fillField('#user','Vinicius' )
    I.fillField('#email','Vinicius_cleber@hotmail.com' )
    I.fillField('#password','123456' )
    I.click('#btnRegister')
    I.waitForText('Cadastro realizado!',3)

}).tag('@Teste0')

Scenario('Cadastro sem nome',  ({ I }) => {
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Cadastro')
    I.fillField('#email','Vinicius_cleber@hotmail.com' )
    I.fillField('#password','123456' )
    I.click('#btnRegister')
    I.waitForText('O campo nome deve ser prenchido',3)

}).tag('@Teste1')

Scenario('Cadastro sem email',  ({ I }) => {
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Cadastro')
    I.fillField('#user','Vinicius' )
    I.fillField('#password','123456' )
    I.click('#btnRegister')
    I.waitForText('O campo e-mail deve ser prenchido corretamente',3)

}).tag('@Teste2')

Scenario('Cadastro sem senha',  ({ I }) => {
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Cadastro')
    I.fillField('#user','Vinicius' )
    I.fillField('#email','Vinicius_cleber@hotmail.com' )
    I.click('#btnRegister')
    I.waitForText('O campo senha deve ter pelo menos 6 dígitos',3)

}).tag('@Teste3')

Scenario('Cadastro sem nada',  ({ I }) => {
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Cadastro')
    I.click('#btnRegister')
    I.waitForText('O campo nome deve ser prenchido',3)

}).tag('@Teste4')

